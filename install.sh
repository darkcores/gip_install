mkdir build
mkdir bin
cd build
echo "Cloning JWL repository"
git clone git@bitbucket.org:darkcores/gip_jwl.git
mv gip_jwl GIP_jwl
cd GIP_jwl
make
cd ..
echo "Cloning APP repository"
git clone git@bitbucket.org:darkcores/gip_app.git
mv gip_app GIP_app
cd GIP_app
make
cp bin/release/gip_2015 ../../bin/gip_2015
cd ..
# make install
